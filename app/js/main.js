window.onload = () => {
    document.querySelector(".tabs-panel__link").classList.add('active');
    document.querySelector(".tab-item").style.display = 'block';
}

function menu(){
    let burger = document.querySelector('.hamburger');
    let header_menu = document.querySelector('.nav__menu');
    let body = document.querySelector('body');
    burger.addEventListener('click',  () => {
        burger.classList.toggle('active');
        header_menu.classList.toggle('active');
        body.classList.toggle('lock');
    });
}

menu();

function openTab(ctx, tab) {

    const tabContent = document.getElementsByClassName("tab-item");
    [...tabContent].forEach(e => e.style.display = "none");

    const tabLinks = document.getElementsByClassName("tabs-panel__link");
    [...tabLinks].forEach(e => e.classList.remove('active'));

    document.getElementById(tab).style.display = "block";
    ctx.classList.add('active');
    ScrollTrigger.refresh();
}

function drawLine(){
    if(window.innerWidth > 768){
        const dots = document.querySelectorAll('.schedule__circle');
        const lastIndx = dots.length - 1;
        dots.forEach((e, idx,arr) => {
            if(idx !== lastIndx){
                const line = e.querySelector('.schedule__vertical-line');
                const lineHeight = arr[idx + 1].getBoundingClientRect().top - e.getBoundingClientRect().bottom ;
                if(line) line.style.height = lineHeight - 30 + 'px';
            }
        })
    }
}

window.addEventListener('load', drawLine)
window.addEventListener('resize', drawLine);


function setImgAsBackground() {
    const imgs = document.querySelectorAll('.background-img');

    imgs.forEach(e => {
        const imgSrc = e.src;
        e.parentElement.style.backgroundImage = `url('${imgSrc}')`;
        e.parentElement.classList.add('background-center');
        e.style.display = 'none';
    });
}

setImgAsBackground();

function initSpeakerSlider() {
    const opts = {
        initialSlide:
            document.querySelector('.speakers__slider').querySelectorAll('.swiper-slide').length - 1,
        navigation: {
            nextEl: ".speakers__next",
            prevEl: ".speakers__prev",
        },
        breakpoints: {
            320:{
                spaceBetween: 8,
                slidesPerView:1,
            },
            350: {
                slidesPerView:1.2,
            },
            576: {
                spaceBetween: 16,
                slidesPerView:2,
            },
            768: {
                slidesPerView:1.2,
                spaceBetween:20,
            },
            992: {
                slidesPerView:2,
                spaceBetween:30,
            },
        },


    }
    new Swiper('.speakers__slider', opts);
}
initSpeakerSlider();


function initGallerySliders() {
    const opts = {
        watchSlidesProgress: true,
        loop: true,
        speed: 1200,
        spaceBetween: 8,
        /*loopedSlides:8,*/
        autoplay: {
            delay: 1,
            disableOnInteraction: false,
        },
        breakpoints: {
            320:{
                spaceBetween: 4,
                slidesPerView: 1,
            },
            576:{
                spaceBetween: 8,
                slidesPerView: 'auto',
            },
        },
        on: {
            init: function () {
                const existingSlides = this.slides;
                const visibleSlides = this.visibleSlides.length;
                let slidesTotal = existingSlides.length;
                while(slidesTotal * .8 < visibleSlides * 2){
                    existingSlides.forEach(e => {
                        this.addSlide(slidesTotal++, `<div class="gallery__slide swiper-slide">${e.innerHTML}</div>`);
                    });
                }
            },
        }
    };

    new Swiper('.gallery__slider--top', opts);
    new Swiper('.gallery__slider--bottom', {...opts,
        autoplay: {
            delay: 1,
            disableOnInteraction: false,
            reverseDirection:true,
        },
    });

}
initGallerySliders();

function toggleAccordion(){
    const acc = document.getElementsByClassName("accordion__header");
    const panels = document.querySelectorAll('.accordion__body');

    [...acc].forEach((accordion, accIdx) => {
        accordion.addEventListener('click', function(){
            panels.forEach((panel, panelIdx) => {
                if(panelIdx !== accIdx){
                    panel.style.maxHeight = null;
                    panel.previousElementSibling.classList.remove('active');
                }else{
                    panel.style.maxHeight = panel.style.maxHeight ? null : panel.scrollHeight + "px";
                    panel.previousElementSibling.classList.toggle('active');
                }
            })
        });
    });
}

toggleAccordion();

function scrollToSchedule() {
    const schedule = document.querySelector('.schedule__tabs-content')
    const scrollBtn = document.querySelector('.hero__btn')
    scrollBtn.addEventListener('click', () => {
        schedule.scrollIntoView({ behavior: "smooth"});
    });

}

scrollToSchedule();

function showHeroBlock() {
    const mm = gsap.matchMedia();
    const tl = gsap.timeline({defaults: {ease: 'power2.in'}});

    mm.add('(min-width:992px)', () => {
        tl.fromTo('.hero__title', { opacity: 0, y: -120 }, { duration: .6, opacity: 1, y: 0 })
            .fromTo('.hero__desc', { opacity: 0, y:50}, {duration:1, y:0, opacity: 1 })
            .fromTo('.hero__date', { opacity: 0,x: 60}, {duration: .6, x: 0, opacity: 1 })
            .fromTo('.hero__location', { opacity: 0, x: -60}, {duration: .6, x: 0, opacity: 1 })
            .fromTo('.hero__btn',{ opacity: 0, y: 40 }, { duration: .6, opacity: 1, y: 0, ease: 'bounce' })
    })
    mm.add('(min-width:320px) and (max-width:991.98px)', () => {
        tl.fromTo('.hero__title', { opacity:0, y: -80}, { duration: .5,opacity:1, y: 0 })
            .fromTo('.hero__desc', { opacity: 0, y: 40}, {duration: .9, y: 0, opacity: 1 })
            .fromTo('.hero__date', { opacity: 0, x: 50}, {duration: .5, x: 0, opacity: 1 })
            .fromTo('.hero__location', { opacity: 0, x: -50}, {duration: .5, x:0, opacity: 1 })
            .fromTo('.hero__btn',{ opacity: 0, y: 40 }, { duration: .6, opacity: 1, y: 0, ease: 'bounce' })
    })
}
showHeroBlock()

function showCommunityBlock() {

    const mm = gsap.matchMedia();
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: '.community__title',
            start: 'top 60%',
        },
        defaults: {opacity:0, ease: 'power2.in'},
    });
    mm.add('(min-width:992px)', () => {
        tl.fromTo('.community__title', { y:-120}, { duration:.6, y:0, opacity:1})
            .fromTo('.community__card', { y:-50, opacity: 0}, {duration:.8, y:0, opacity:1, stagger:.5})
    });
    mm.add('(min-width:768px) and (max-width:991.98px)', () => {
        tl.fromTo('.community__title', { y:-80}, { duration:.5, y:0, opacity:1})
            .fromTo('.community__card', { y:-40, opacity: 0}, {duration:.7, y:0, opacity:1, stagger:.5})
    })
    mm.add('(max-width:767.98px)', () => {
        tl.fromTo('.community__title', { y:-60}, { duration:.4, y:0, opacity:1})
            .fromTo('.community__card', { y:-30, opacity: 0}, {duration:.6, y:0, opacity:1, stagger:.5})
    });
}
showCommunityBlock()


function showMeetupBlock() {
    const mm = gsap.matchMedia();
    const imgTl = gsap.timeline({
        scrollTrigger:{
            trigger: '.meetup__images',
            start: 'top center',

        },
        defaults:{ease: 'power2.in'},
    });

    const txtTl = gsap.timeline({
        scrollTrigger:{
            trigger: '.meetup__l-content',
            start: 'top center',
        },
        defaults:{ease: 'power2.in'},
    })
    mm.add('(min-width:992px)', () => {
        imgTl.fromTo('.meetup__img-wrapper', {opacity:0, y:-80}, { duration:.7, y:0, opacity:1, stagger: .4})
            .fromTo('.meetup__show-btn', {opacity:0}, { duration: .4, opacity:1});

        txtTl.fromTo('.meetup__title', { y:-120, opacity:0}, { duration:.6, y:0, opacity:1})
            .fromTo('.meetup__desc', { y: 60, opacity: 0}, {duration:.8, y:0, opacity:1})
            .fromTo('.meetup__date-wrapper', { y: 60, opacity: 0}, {duration:.3, y:0, opacity:1})
            .fromTo('.meetup__location-wrapper', { y: 60, opacity: 0}, {duration:.3, y:0, opacity:1}, '<.3')
            .fromTo('.meetup__ticket', {opacity:0}, {duration:.4, opacity:1})
            .fromTo('.meetup__more-btn', {opacity:0}, {duration:.4, opacity:1})
    });
    mm.add('(min-width:768px) and (max-width:991.98px)', () => {
        imgTl.fromTo('.meetup__img-wrapper', {opacity:0, y:-60}, { duration:.6, y:0, opacity:1, stagger: .3})
            .fromTo('.meetup__show-btn', {}, { duration: .4, opacity:1})

        txtTl.fromTo('.meetup__title', {opacity:0, y:-80}, { duration:.5, y:0, opacity:1})
            .fromTo('.meetup__desc', { y: 40, opacity: 0}, {duration:.7, y:0, opacity:1})
            .fromTo('.meetup__date-wrapper', { y: 40, opacity: 0}, {duration:.3, y:0, opacity:1})
            .fromTo('.meetup__location-wrapper', { y: 40, opacity: 0}, {duration:.3, y:0, opacity:1}, '<.2')
            .fromTo('.meetup__ticket', {opacity:0}, {duration:.3, opacity:1})
            .fromTo('.meetup__more-btn', {opacity:0}, {duration:.3, opacity:1})
    })

    mm.add('(max-width:767.98px)', () => {
        imgTl.fromTo('.meetup__img-wrapper', {opacity:0, y:-40}, { duration:.5, y:0, opacity:1, stagger: .3})
            .fromTo('.meetup__show-btn', {opacity:0}, { duration: .4, opacity:1})

        txtTl.fromTo('.meetup__title', {opacity:0, y:-30}, { duration:.3, y:0, opacity:1})
            .fromTo('.meetup__desc', { y: 30, opacity: 0}, {duration:.6, y:0, opacity:1})
            .fromTo('.meetup__date-wrapper', { y: 30, opacity: 0}, {duration:.3, y:0, opacity:1})
            .fromTo('.meetup__location-wrapper', { y: 30, opacity: 0}, {duration:.3, y:0, opacity:1}, '<.2')
            .fromTo('.meetup__ticket', {opacity:0}, {duration:.3, opacity:1})
            .fromTo('.meetup__more-btn', {opacity:0}, {duration:.3, opacity:1})
    })
}
showMeetupBlock()


function showScheduleBlock() {
    window.addEventListener('load', () => {

        const events = gsap.utils.toArray(".schedule__event");

        events.forEach(event => {
            gsap.timeline({
                scrollTrigger:{
                    trigger: event,
                    start: 'top center',
                },
                defaults:{ opacity:0, ease: 'power2.in'},
            }).fromTo(event.querySelector('.schedule__event-desc'), { opacity:0}, {duration:.5, opacity:1})
                .fromTo(event.querySelector('.schedule__time-wrapper'), { opacity:0}, {duration:.5, opacity:1})
        })
    });
}

showScheduleBlock();

function showSpeakersBlock() {
    window.addEventListener('load', () => {
        const tl = gsap.timeline({
            scrollTrigger:{
                trigger:'.speakers__txt-inner',
                start: 'top center',
                /*markers: {startColor: "black", endColor: "orange", fontSize: "25px", fontWeight: "bold", indent: 200}*/
            },
            defaults:{
                opacity:0,
                ease: 'power2.in',
            }
        });

        tl.fromTo('.speakers__title', {opacity:0, y: -60}, {duration: .7, y: 0, opacity: 1})
            .fromTo('.speakers__desc', {y:60}, {duration: .7, y:0, opacity:1})
            .fromTo('.speakers__all-btn', {}, {opacity: 1, duration: .4})
            .fromTo('.speakers__nav-btn', {opacity: 0}, {opacity: 1, duration: .4, stagger: .3})
            .fromTo('.speakers__slide-outer', {y: -40, opacity: 0}, {opacity: 1, y:0, duration: .5,
                stagger: { from: 'end', each: .4 }
            });
    });
}
showSpeakersBlock();


function showDesignersBlock() {
    window.addEventListener('load', () => {
        const mm = gsap.matchMedia();
        const tl = gsap.timeline({
            scrollTrigger:{
                trigger:'.designers__header',
                start: 'top 40%',
            },
            defaults:{
                ease: 'power2.in',
            }
        });

        mm.add('(min-width: 992px)', () => {
            tl.fromTo('.designers__subtitle', {opacity:0 }, {duration: .7, opacity: 1})
                .fromTo('.designers__title', {opacity:0, y:60}, {duration: .5, y:0, opacity:1})
                .fromTo('.designers__card', {opacity:0}, {duration:.7, opacity:1, stagger:.5})
        });

        mm.add('(min-width:320px) and (max-width: 991.98px)', () => {
            tl.fromTo('.designers__subtitle', {opacity:0 }, {duration: .6, opacity: 1})
                .fromTo('.designers__title', {opacity:0, y: 30}, {duration: .3, y: 0, opacity: 1})

            const cards = gsap.utils.toArray('.designers__card');
            cards.forEach(card => {
                gsap.set(card, {
                    opacity: 0,
                    })
                gsap.to(card, {
                    scrollTrigger: {
                        trigger: card,
                        start: 'top center',
                    },
                    y: 0,
                    duration: .8,
                    opacity: 1,
                })
            })
        });

    })
}
showDesignersBlock()

function showVenueBlock() {
    window.addEventListener('load', () => {
        const mm = gsap.matchMedia();

        const imgTl = gsap.timeline({
            scrollTrigger:{
                trigger:'.venue__pictures',
                start: 'top center'
            },
            defaults:{
                ease: 'power2.in',
            }
        });
        imgTl.fromTo('.venue__img-outer',
            {opacity:0, y:-60},
            {duration: .8, opacity:1, y:0, stagger: .5})

        const txtTl = gsap.timeline({
            scrollTrigger:{
                trigger:'.venue__txt',
                start: 'top center'
            },
            defaults:{
                ease: 'power2.in',
            }
        })
        mm.add('(min-width:768px)', () => {
            txtTl.fromTo('.venue__title', {opacity:0, y:-60}, {y:0, opacity:1, duration: .7})
                .fromTo('.venue__subtitle', {opacity:0, y:40}, {y:0, opacity:1, duration: .6})
                .fromTo('.venue__desc', {opacity:0}, {opacity:1, duration: .5})
                .fromTo('.venue__btn-txt', {opacity:0, y:40}, {opacity:1, y:0, duration:.5})
                .fromTo('.venue__btn', {opacity:0, y:40}, {opacity:1, y:0, duration:.5, stagger:.4})
        })
        mm.add('(max-width: 767.98px)', () => {
            txtTl.fromTo('.venue__title', {opacity:0, y:0, x:40}, {x:0, opacity:1, duration: .6})
                .fromTo('.venue__subtitle', {opacity:0, y:0, x:40}, {x:0, opacity:1, duration: .6})
                .fromTo('.venue__desc', {opacity:0}, {opacity:1, duration: .5})
                .fromTo('.venue__btn-txt', {opacity:0, y:25}, {opacity:1, y:0, duration:.4})
                .fromTo('.venue__btn', {opacity:0, y:25}, {opacity:1, y:0, duration:.4, stagger:.3})
        })
    });
}
showVenueBlock()

function showSponsorsBlock() {
    window.addEventListener('load', () => {
        const mm = gsap.matchMedia();
        const headerTl =  gsap.timeline({
            scrollTrigger:{
                trigger:'.sponsors__header',
                start: 'top center'
            },
            defaults:{
                ease: 'power2.in',
            }
        });
        headerTl.fromTo('.sponsors__title', {opacity:0, y: -40},{opacity:1, y:0, duration:.6})
        const tl = gsap.timeline({
            scrollTrigger:{
                trigger:'.sponsors__items',
                start: 'top 55%'
            },
            defaults:{
                ease: 'power2.in',
            }
        });

        mm.add('(min-width:576px', () => {
            tl.fromTo('.sponsors__item', {opacity:0, x: 40}, {opacity:1, x:0, duration:.4, stagger: .3})
                .fromTo('.sponsors__btn', {opacity:0, y:40}, {opacity:1, y:0, duration:.6})
        })
        mm.add('(min-width: 320px) and (max-width: 575.98px)', () => {
            tl.fromTo('.sponsors__item', {opacity:0, x: 0}, {opacity:1, duration:.3, stagger: .2})
                .fromTo('.sponsors__btn', {opacity:0}, {opacity:1, y:0, duration:.6})
        })
    });
}

showSponsorsBlock()

/*function showGalleryBlock() {
    window.addEventListener('load', () => {
        gsap.timeline({scrollTrigger:{
                trigger:'.gallery__title',
                start: 'top center'
            }
        }).fromTo('.gallery__title', { opacity: 0, y: -40 }, { opacity: 1, y: 0, duration: .7 });

        const sliders = gsap.utils.toArray('.gallery__slider')
        sliders.forEach(slider => {
            gsap.timeline({scrollTrigger: {
                    trigger: slider,
                    start: 'top center'
                }
            }).fromTo(slider, {opacity:0}, {opacity:1, duration:.7})
        })
    });
}

showGalleryBlock()*/

function showFormBlock() {
    window.addEventListener('load', () => {
        gsap.timeline({scrollTrigger:{
                trigger:'.footer__subscription',
                start: 'top center'
            }
        }).fromTo('.footer__subscription', { opacity: 0}, { opacity: 1,duration: .7 });
    })
}

showFormBlock()